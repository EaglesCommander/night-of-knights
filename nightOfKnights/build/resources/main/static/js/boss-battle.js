$(document).ready(function(){
    checkGameStatus();
});

function checkGameStatus() {
    redirectIfKnightIsDead();
    redirectIfBossIsDead();
}

function redirectIfKnightIsDead() {
    $.get("knight-status/", function(status) {
        if (status == true) {
            $(location).attr('href', window.location.href + "defeat/");
        }
    });
}

function redirectIfBossIsDead() {
    $.get("boss-status/", function(status) {
        if (status == true) {
            $(location).attr('href', window.location.href + "victory/");
        }
    });
}

function attack() {
    $.get("attack/", function(fragment) { // get from controller
        $("#actionPart").replaceWith(fragment); // update snippet of page
    });

    checkGameStatus();
}

function defend() {
    $.get("defend/", function(fragment) { // get from controller
        $("#actionPart").replaceWith(fragment); // update snippet of page
    });

    checkGameStatus();
}

function skill() {
    $.get("skill/", function(fragment) { // get from controller
        $("#actionPart").replaceWith(fragment); // update snippet of page
    });

    checkGameStatus();
}

function record() {
    $(location).attr('href', window.location.href + "record/")
}