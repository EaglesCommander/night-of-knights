package id.ac.ui.cs.advprog.nightofknights.core.gamechar;

import id.ac.ui.cs.advprog.nightofknights.core.stats.*;

public interface GameChar {
    public Stats getCharStats();

    public void setCharStats(Stats charStats);

    public void resetCharStats();
}