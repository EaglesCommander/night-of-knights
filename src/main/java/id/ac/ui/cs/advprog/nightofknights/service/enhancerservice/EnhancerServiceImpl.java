package id.ac.ui.cs.advprog.nightofknights.service.enhancerservice;

import id.ac.ui.cs.advprog.nightofknights.core.enhancer.*;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;
import org.springframework.stereotype.Service;

@Service
public class EnhancerServiceImpl implements EnhancerService {
    private Equipment eq;

    public EnhancerServiceImpl(Equipment eq) {
        this.eq = eq;
    }

    /**
     * Upgrade equipment using Light.
     * @param eq is the equipment.
     * @return upgraded equipment.
     */
    public Equipment equipmentLightUpgrade(Equipment eq) {
        Equipment upgraded = new LightUpgrade(eq);
        this.eq = upgraded;
        return this.eq;
    }

    /**
     * Upgrade equipment using Dark.
     * @param eq is the equipment.
     * @return upgraded equipment.
     */
    public Equipment equipmentDarkUpgrade(Equipment eq) {
        Equipment upgraded = new DarkUpgrade(eq);
        this.eq = upgraded;
        return this.eq;
    }

    public String equipmentGetName() {
        return this.eq.getName();
    }

    public int equipmentGetValue() {
        return this.eq.getValue();
    }

    public String equipmentGetDesc() {
        return this.eq.getDescription();
    }
}
