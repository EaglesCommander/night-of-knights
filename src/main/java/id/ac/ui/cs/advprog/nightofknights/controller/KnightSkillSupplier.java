package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.service.battleservice.BattleServiceImpl;
import java.util.function.Supplier;

public class KnightSkillSupplier implements Supplier<String> {

    private BattleServiceImpl battleService;

    public KnightSkillSupplier(BattleServiceImpl battleService) {
        this.battleService = battleService;
    }

    @Override
    public String get() {
        return battleService.knightSkill();
    }
}