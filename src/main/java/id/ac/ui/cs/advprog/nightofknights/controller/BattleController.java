package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.core.gamechar.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import id.ac.ui.cs.advprog.nightofknights.feign.HistoryFeign;
import id.ac.ui.cs.advprog.nightofknights.service.battleservice.BattleServiceImpl;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.BossServiceImpl;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightServiceImpl;
import java.util.*;
import java.util.concurrent.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Controller
public class BattleController {
    @Autowired
    private RestTemplate restTemplate;

    private final KnightServiceImpl knightService;
    private final BossServiceImpl bossService;
    private final BattleServiceImpl battleService;
    private final HistoryFeign historyFeign;

    private static final String KNIGHTLOG = "knightLog";
    private static final String BOSSLOG = "bossLog";
    private static final String ACTIONPART = "boss-battle :: #actionPart";

    /**
     * Constructor for battlecontroller.
     * @param knightService is for connecting with knight.
     * @param bossService is for connecting with boss.
     * @param battleService is for the battles.
     */
    public BattleController(KnightServiceImpl knightService, BossServiceImpl bossService,
                            BattleServiceImpl battleService, HistoryFeign historyFeign) {
        this.knightService = knightService;
        this.bossService = bossService;
        this.battleService = battleService;
        this.historyFeign = historyFeign;
    }

    /**
     * Function to update the model with needed attributes.
     * @param model is the models of other urls.
     * @return returns the added model back.
     */
    public ModelMap updateModelMap(ModelMap model) {
        model.addAttribute("knight", knightService.getKnight());
        model.addAttribute("boss", bossService.getBoss());
        model.addAttribute("knightStats", knightService.getKnightCharStats());
        model.addAttribute("bossStats", bossService.getBossStats());

        return model;
    }

    /**
     * Function for accessing the battle page naturally.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/boss-battle/")
    public String battleHome(Model model) {
        battleService.access();

        model.addAttribute("knight", knightService.getKnight());
        model.addAttribute("boss", bossService.getBoss());
        model.addAttribute("knightStats", knightService.getKnightCharStats());
        model.addAttribute("bossStats", bossService.getBossStats());
        model.addAttribute(KNIGHTLOG, "");
        model.addAttribute(BOSSLOG, "");

        return "boss-battle";
    }

    /**
     * Function that is called after clicking the attack button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/boss-battle/attack/")
    public String attackButton(ModelMap model) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CompletableFuture<String> knightLog = CompletableFuture.supplyAsync(
                new KnightAttackSupplier(battleService), executorService);
        CompletableFuture<String> bossLog = CompletableFuture.supplyAsync(
                new BossAttackSupplier(battleService), executorService);

        try {
            model.addAttribute(KNIGHTLOG, knightLog.get());
            model.addAttribute(BOSSLOG, bossLog.get());
        } catch (Exception e) {
            model.addAttribute(KNIGHTLOG, "No weapon equipped - Create char first!");
        }
        updateModelMap(model);

        return ACTIONPART;
    }

    /**
     * Function that is called after clicking the defend button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/boss-battle/defend/")
    public String defendButton(ModelMap model) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CompletableFuture<String> knightLog = CompletableFuture.supplyAsync(
                new KnightDefendSupplier(battleService), executorService);
        CompletableFuture<String> bossLog = CompletableFuture.supplyAsync(
                new BossAttackSupplier(battleService), executorService);

        try {
            model.addAttribute(KNIGHTLOG, knightLog.get());
            model.addAttribute(BOSSLOG, bossLog.get());
        } catch (Exception e) {
            model.addAttribute(KNIGHTLOG, "No armor equipped - Create char first!");
        }

        updateModelMap(model);

        return ACTIONPART;
    }

    /**
     * Function that is called after clicking the skill button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/boss-battle/skill/")
    public String skillButton(ModelMap model) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CompletableFuture<String> knightLog = CompletableFuture.supplyAsync(
                new KnightSkillSupplier(battleService), executorService);
        CompletableFuture<String> bossLog = CompletableFuture.supplyAsync(
                new BossAttackSupplier(battleService), executorService);

        try {
            model.addAttribute(KNIGHTLOG, knightLog.get());
            model.addAttribute(BOSSLOG, bossLog.get());
        } catch (Exception e) {
            model.addAttribute(KNIGHTLOG, "No skill equipped - Create char first!");
        }

        updateModelMap(model);

        return ACTIONPART;
    }

    /**
     * Function to retreive the view of the victory page.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/boss-battle/victory/")
    public String battleVictory(ModelMap model) {
        updateModelMap(model);

        return "victory";
    }

    /**
     * Function to retreive the view of the defeat page.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/boss-battle/defeat/")
    public String battleDefeat(ModelMap model) {
        updateModelMap(model);

        return "defeat";
    }

    @GetMapping(path = "/boss-battle/knight-status/", produces = "application/json")
    @ResponseBody
    public boolean knightStatus() {
        return knightService.isKnightDead();
    }

    @GetMapping(path = "/boss-battle/boss-status/", produces = "application/json")
    @ResponseBody
    public boolean bossStatus() {
        return bossService.isBossDead();
    }

    /**
     * A function to communicate with History-Service to store entry.
     * @return a redirection to the history page.
     */
    @GetMapping("/record/")
    public String battleDone() {
        Knight knight = (Knight) knightService.getKnight();
        BossStats bossStats = (BossStats) bossService.getBossStats();
        String status = "Victory";

        if (knightStatus()) {
            status = "Defeat";
        }

        if (!knight.gameOver) {
            knight.gameOver = true;
            ArrayList<String> entry = new ArrayList<String>();
            entry.add(knight.getName());
            entry.add(status);
            entry.add(Integer.toString(bossStats.getLevel()));
            historyFeign.addHistory(entry);
        }

        return "redirect:/history/";
    }
}
