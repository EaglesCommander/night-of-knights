package id.ac.ui.cs.advprog.nightofknights.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/menu")
public class MainMenuController {

    @GetMapping("/")
    public String menu() {
        return "mainMenu";
    }
}
