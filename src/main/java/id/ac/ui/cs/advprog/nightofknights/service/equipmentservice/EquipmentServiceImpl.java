package id.ac.ui.cs.advprog.nightofknights.service.equipmentservice;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.repository.EquipmentRepository;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EquipmentServiceImpl implements EquipmentService {
    @Autowired
    KnightService knightService;

    private final EquipmentRepository equipmentRepository;

    public EquipmentServiceImpl() {
        equipmentRepository = new EquipmentRepository();
    }

    /**
     * Function to create equipment that will be used by selected knight.
     * @param knightName name of the knight
     * @param knightType class of the knight
     */
    public void createChar(String knightName,String knightType) {
        Equipment[] knightEquipments = createCharEquipment(knightType);
        Equipment[] knightStorage = createCharStorage(knightType);
        knightService.createKnight(
                knightName,knightType,knightEquipments[0],knightEquipments[1],knightEquipments[2]);
        knightService.getKnightStorage().put("weaponStorage", knightStorage[0]);
        knightService.getKnightStorage().put("skillStorage", knightStorage[1]);
        knightService.getKnightStorage().put("armorStorage", knightStorage[2]);
    }

    /**
     * Function to create equipment that will be used by selected knight.
     * @return returns a list of equipment(weapon, skill, and armor).
     */
    @Override
    public Equipment[] createCharEquipment(String type) {
        return equipmentRepository.createCharacter(type);
    }

    @Override
    public Equipment[] createCharStorage(String type) {
        return equipmentRepository.createStorage(type);
    }
}
