package id.ac.ui.cs.advprog.nightofknights.feign;

import java.util.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "HISTORY-SERVICE")
public interface HistoryFeign {
    @GetMapping("/get/")
    @ResponseBody
    public String getHistory();

    @PostMapping("/add/")
    @ResponseBody
    public void addHistory(@RequestBody ArrayList<String> entryList);
}
