package id.ac.ui.cs.advprog.nightofknights.core.stats;

public interface Stats {
    public int getMaxHealth();

    public int getCurrentHealth();
}
