package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.AntiqueCuirass;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.VioletRequiem;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.HunterKnife;
import id.ac.ui.cs.advprog.nightofknights.core.stats.KnightStats;
import id.ac.ui.cs.advprog.nightofknights.core.stats.Stats;
import id.ac.ui.cs.advprog.nightofknights.service.enhancerservice.EnhancerServiceImpl;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/shop")
public class EnhancerController {
    private KnightStats knightStats;

    @Autowired
    private EnhancerServiceImpl enhancerService;

    @Autowired
    private KnightServiceImpl knightService;

    public EnhancerController(EnhancerServiceImpl enhancerService,
                              KnightServiceImpl knightService) {
        this.enhancerService = enhancerService;
        this.knightService = knightService;
    }

    private void reduceExp() {
        knightStats.addExperience(-50);
    }

    /**
     * Get knight exp.
     * @return exp if not null, else 0.
     */
    public int getKnightExp() {
        if (knightStats != null) {
            return knightStats.getExperience();
        } else {
            return 0;
        }
    }

    /**
     * Function to update the model with needed attributes.
     * @param model is the models of other urls.
     * @return returns the added model back.
     */
    public ModelMap updateModelMap(ModelMap model) {
        model.addAttribute("knight", knightService.getKnight());
        model.addAttribute("knightStats", knightService.getKnightCharStats());
        model.addAttribute("weapon", knightService.getKnightWeapon());
        model.addAttribute("armor", knightService.getKnightArmor());
        model.addAttribute("skill", knightService.getKnightSkill());

        return model;
    }

    /**
     * Function when accessing shop screen.
     * @param model is the models for mvc.
     * @return shop.html.
     */
    @GetMapping("/")
    public String shopScreen(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();

        knightService.setKnightWeapon(knightService.getKnightWeapon());
        knightService.setKnightArmor(knightService.getKnightArmor());
        knightService.setKnightSkill(knightService.getKnightSkill());

        model.addAttribute("knight", knightService.getKnight());
        model.addAttribute("knightStats", knightService.getKnightCharStats());
        model.addAttribute("weapon", knightService.getKnightWeapon());
        model.addAttribute("armor", knightService.getKnightArmor());
        model.addAttribute("skill", knightService.getKnightSkill());

        return "shop";
    }

    /**
     * Function that is called after clicking the weapon Dark Upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/weapon/dark/")
    public String weaponDarkUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            Equipment darkUpgradedWeapon =
                    enhancerService.equipmentDarkUpgrade(knightService.getKnightWeapon());
            model.addAttribute("weapon", darkUpgradedWeapon);
            reduceExp();
            updateModelMap(model);
            knightService.setKnightWeapon(darkUpgradedWeapon);
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the weapon Light Upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/weapon/light/")
    public String weaponLightUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            Equipment lightUpgradedWeapon =
                    enhancerService.equipmentLightUpgrade(knightService.getKnightWeapon());
            model.addAttribute("weapon", lightUpgradedWeapon);
            reduceExp();
            updateModelMap(model);
            knightService.setKnightWeapon(lightUpgradedWeapon);
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the armor Dark Upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/armor/dark/")
    public String armorDarkUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            Equipment darkUpgradedArmor =
                    enhancerService.equipmentDarkUpgrade(knightService.getKnightArmor());
            model.addAttribute("armor", darkUpgradedArmor);
            reduceExp();
            updateModelMap(model);
            knightService.setKnightArmor(darkUpgradedArmor);
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the armor Light Upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/armor/light/")
    public String armorLightUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            Equipment lightUpgradedArmor =
                    enhancerService.equipmentLightUpgrade(knightService.getKnightArmor());
            model.addAttribute("armor", lightUpgradedArmor);
            reduceExp();
            updateModelMap(model);
            knightService.setKnightArmor(lightUpgradedArmor);
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the skill Dark Upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/skill/dark/")
    public String skillDarkUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            Equipment darkUpgradedSkill =
                    enhancerService.equipmentDarkUpgrade(knightService.getKnightSkill());
            model.addAttribute("skill", darkUpgradedSkill);
            reduceExp();
            updateModelMap(model);
            knightService.setKnightSkill(darkUpgradedSkill);
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the skill Light Upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/skill/light/")
    public String skillLightUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            Equipment lightUpgradedSkill =
                    enhancerService.equipmentLightUpgrade(knightService.getKnightSkill());
            model.addAttribute("skill", lightUpgradedSkill);
            reduceExp();
            updateModelMap(model);
            knightService.setKnightSkill(lightUpgradedSkill);
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the health upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/health/")
    public String healthUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            knightStats.setMaxHealth(knightStats.getMaxHealth() + 25);
            reduceExp();
        }

        return "redirect:/shop/";
    }

    /**
     * Function that is called after clicking the mana upgrade button.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/upgrade/mana/")
    public String manaUpgradeButton(ModelMap model) {
        knightStats = (KnightStats)knightService.getKnightCharStats();
        if (getKnightExp() >= 50) {
            knightStats.setMaxMana(knightStats.getMaxMana() + 10);
            reduceExp();
        }

        return "redirect:/shop/";
    }
}
