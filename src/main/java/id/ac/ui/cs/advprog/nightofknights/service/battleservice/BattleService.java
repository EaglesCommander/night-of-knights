package id.ac.ui.cs.advprog.nightofknights.service.battleservice;


public interface BattleService {
    void updateCheck();

    void access();

    String knightAttack();

    String knightDefend();

    String knightSkill();

    String bossAttack();
}
