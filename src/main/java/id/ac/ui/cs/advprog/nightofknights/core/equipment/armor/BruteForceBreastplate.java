package id.ac.ui.cs.advprog.nightofknights.core.equipment.armor;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class BruteForceBreastplate extends Equipment {

    /** Concrete class of armor BruteForceBreastplate.*/

    public BruteForceBreastplate() {
        this.name = "Brute Force Breastplate";
        this.desc = "This is Brute Force Breastplate";
        this.value = 5;
    }

}
