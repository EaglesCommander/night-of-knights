package id.ac.ui.cs.advprog.nightofknights.service.gamecharservice;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;
import id.ac.ui.cs.advprog.nightofknights.core.gamechar.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class KnightServiceImpl implements KnightService {
    private Knight knight;

    public KnightServiceImpl() {
        this.knight = Knight.getInstance();
    }

    public GameChar getKnight() {
        return this.knight;
    }

    public int knightAttack() {
        return this.knight.attack();
    }

    public int knightSkill() {
        return this.knight.skill();
    }

    public int knightDefend() {
        return this.knight.defend();
    }

    public void setKnightWeapon(Equipment weapon) {
        this.knight.setWeapon(weapon);
    }

    public void setKnightSkill(Equipment skill) {
        this.knight.setSkill(skill);
    }

    public void setKnightArmor(Equipment armor) {
        this.knight.setArmor(armor);
    }

    public void setKnightCharStats(Stats charStats) {
        this.knight.setCharStats(charStats);
    }

    public Equipment getKnightWeapon() {
        return this.knight.getWeapon();
    }

    public Equipment getKnightSkill() {
        return this.knight.getSkill();
    }

    public Equipment getKnightArmor() {
        return this.knight.getArmor();
    }

    public Stats getKnightCharStats() {
        return this.knight.getCharStats();
    }

    public String getKnightName() {
        return this.knight.getName();
    }

    public String getKnightClass() {
        return this.knight.getKnightClass();
    }

    public void createKnight(String name, String knightClass,
                             Equipment weapon, Equipment skill, Equipment armor) {
        this.knight.createKnight(name, knightClass, weapon, skill, armor);
    }

    public void resetKnightInstance() {
        Knight.deleteInstance();
        this.knight = Knight.getInstance();
    }

    /**
     * A function that checks if the knight currently has 0 or less health.
     * @return boolean, true if dead, false if alive.
     */
    public boolean isKnightDead() {
        Stats knightStats = getKnightCharStats();

        if (knightStats.getCurrentHealth() <= 0) {
            return true;
        }

        return false;
    }

    public Map<String, Equipment> getKnightStorage() {
        return this.knight.getStorage();
    }
}
