package id.ac.ui.cs.advprog.nightofknights.service.gamecharservice;

import id.ac.ui.cs.advprog.nightofknights.core.gamechar.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;

public interface BossService {
    GameChar getBoss();

    Stats getBossStats();

    void setBossStats(Stats charStats);

    void resetBossInstance();

    void resetBossCharStats();

    boolean isBossDead();
}
