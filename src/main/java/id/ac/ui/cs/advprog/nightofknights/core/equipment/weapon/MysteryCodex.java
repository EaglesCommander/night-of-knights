package id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;


public class MysteryCodex extends Equipment {
    /** Concrete class of weapon MysteryCodex.*/
    public MysteryCodex() {
        this.name = "Mystery Codex";
        this.desc = "This is Mystery Codex";
        this.value = 60;
    }
}
