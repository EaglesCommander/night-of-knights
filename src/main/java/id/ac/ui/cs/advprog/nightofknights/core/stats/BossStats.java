package id.ac.ui.cs.advprog.nightofknights.core.stats;

public class BossStats implements Stats {
    private int maxHealth;
    private int currentHealth;
    private int level;

    /**
     * Constructor for BossStats, places max value of int as default and 0 as default level.
     */
    public BossStats() {
        this.maxHealth = 100000;
        this.currentHealth = maxHealth;
        this.level = 0;
    }

    public int getMaxHealth() {
        return this.maxHealth;
    }

    public int getCurrentHealth() {
        return this.currentHealth;
    }

    public int getLevel() {
        return this.level;
    }

    public void increaseCurrentHealth(int amount) {
        this.currentHealth += amount;
    }

    public void increaseLevel(int amount) {
        this.level += amount;
    }
}
