package id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class Dagger extends Equipment {
    /** Concrete class of weapon Dagger.*/
    public Dagger() {
        this.name = "Dagger";
        this.desc = "This is Dagger";
        this.value = 65;
    }
}
