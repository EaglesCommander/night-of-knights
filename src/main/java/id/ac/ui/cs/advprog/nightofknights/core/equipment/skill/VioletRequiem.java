package id.ac.ui.cs.advprog.nightofknights.core.equipment.skill;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class VioletRequiem extends Equipment {

    /** Concrete class of skill VioletRequiem.*/

    public VioletRequiem() {
        this.name = "Violet Requiem";
        this.desc = "This is Violet Requiem";
        this.value = 910;
    }
}
