package id.ac.ui.cs.advprog.nightofknights.core.gamechar;

import id.ac.ui.cs.advprog.nightofknights.core.stats.*;

public class Boss implements GameChar {
    private Stats charStats;

    private static Boss instance;

    private Boss() {
        this.charStats = new BossStats();
    }

    /**
     * Singleton method to return the instance of Boss or make a new one.
     *
     * @return returns a boss instance.
     */
    public static Boss getInstance() {
        if (instance == null) {
            instance = new Boss();
        }

        return instance;
    }

    public Stats getCharStats() {
        return this.charStats;
    }

    public void setCharStats(Stats charStats) {
        this.charStats = charStats;
    }

    public void resetCharStats() {
        this.charStats = new BossStats();
    }

    public static void deleteInstance() {
        Boss.instance = null;
    }
}
