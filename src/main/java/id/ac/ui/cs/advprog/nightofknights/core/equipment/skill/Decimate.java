package id.ac.ui.cs.advprog.nightofknights.core.equipment.skill;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class Decimate extends Equipment {
    /** Concrete class of skill Decimate.*/
    public Decimate() {
        this.name = "Decimate";
        this.desc = "This is Decimate";
        this.value = 850;
    }
}
