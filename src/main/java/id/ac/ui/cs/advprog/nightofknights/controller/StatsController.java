package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.TwilightArmor;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.Decimate;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.Dagger;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/stats")
public class StatsController {
    private final KnightServiceImpl knightService;

    public StatsController(KnightServiceImpl knightService) {
        this.knightService = knightService;
    }

    /**
     * Function to update the model with needed attributes.
     *
     * @param model is the models of other urls.
     * @return returns the added model back.
     */
    public ModelMap updateModelMap(ModelMap model) {
        model.addAttribute("knight", knightService.getKnight());
        model.addAttribute("knightStats", knightService.getKnightCharStats());
        model.addAttribute("weapon", knightService.getKnightWeapon());
        model.addAttribute("armor", knightService.getKnightArmor());
        model.addAttribute("skill", knightService.getKnightSkill());

        return model;
    }

    /**
     * Function for accessing the battle page naturally.
     *
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping("/")
    public String statsHome(Model model) {
        model.addAttribute("knight", knightService.getKnight());
        model.addAttribute("knightStats", knightService.getKnightCharStats());
        model.addAttribute("weapon", knightService.getKnightWeapon());
        model.addAttribute("armor", knightService.getKnightArmor());
        model.addAttribute("skill", knightService.getKnightSkill());

        return "statsScreen";
    }

    /**
     * Function to change equipment set for the knight.
     *
     * @param model is the models for mvc
     * @return returns a view.
     */
    @GetMapping("/change")
    public String changeEquipments(ModelMap model) {
        Equipment pickedWeapon = knightService.getKnightStorage().get("weaponStorage");
        Equipment pickedSkill = knightService.getKnightStorage().get("skillStorage");
        Equipment pickedArmor = knightService.getKnightStorage().get("armorStorage");

        model.addAttribute("weapon", pickedWeapon);
        model.addAttribute("skill", pickedSkill);
        model.addAttribute("armor", pickedArmor);

        updateModelMap(model);

        knightService.getKnightStorage().clear();

        knightService.getKnightStorage().put("weaponStorage", knightService.getKnightWeapon());
        knightService.getKnightStorage().put("skillStorage", knightService.getKnightSkill());
        knightService.getKnightStorage().put("armorStorage", knightService.getKnightArmor());

        knightService.setKnightWeapon(pickedWeapon);
        knightService.setKnightSkill(pickedSkill);
        knightService.setKnightArmor(pickedArmor);

        return "redirect:/stats/";
    }
}
