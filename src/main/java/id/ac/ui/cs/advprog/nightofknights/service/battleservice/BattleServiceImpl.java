package id.ac.ui.cs.advprog.nightofknights.service.battleservice;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.*;
import java.util.Random;
import org.springframework.stereotype.Service;


@Service
public class BattleServiceImpl implements BattleService {
    private KnightService knightService;
    private BossService bossService;
    private KnightStats knightStats;
    private BossStats bossStats;
    private Random random = new Random();

    public BattleServiceImpl() {
        knightService = new KnightServiceImpl();
        bossService = new BossServiceImpl();
    }

    public void updateCheck() {
        this.knightStats = (KnightStats) knightService.getKnightCharStats();
        this.bossStats = (BossStats) bossService.getBossStats();
    }

    /**
     * Function to increase the boss level by 1.
     */
    public void access() {
        updateCheck();

        bossStats.increaseLevel(1);
        knightStats.changeHealth(knightStats.getMaxHealth() - knightStats.getCurrentHealth());
        knightStats.changeMana(knightStats.getMaxMana() - knightStats.getCurrentMana());
    }

    /**
     * Function to edit the boss' health due to knight's attack.
     * @return returns a log of the knight's attack.
     */
    public String knightAttack() {
        updateCheck();

        int damage = knightService.knightAttack() + random.nextInt(5);
        bossStats.increaseCurrentHealth(damage * -1);
        knightStats.addExperience(damage / 10);

        return "You dealt " + damage + " to the boss";
    }

    /**
     * Function to edit the knight's health due to knight's defense.
     * @return returns a log of the knight's defense.
     */
    public String knightDefend() {
        updateCheck();

        int mitigation = knightService.knightDefend();
        knightStats.changeHealth(mitigation);
        knightStats.changeMana(mitigation / 4);

        return "You recovered " + mitigation + " amount of damage and " + mitigation / 4 + " mana";
    }

    /**
     * Function to edit either the boss' or the knight's parameter due a skill.
     * @return returns a log of the knight's skill.
     */
    public String knightSkill() {
        updateCheck();

        if (knightStats.getCurrentMana() > 30) {
            knightStats.changeMana(-30);
            Equipment skill = knightService.getKnightSkill();
            int skillEffect = knightService.knightSkill();
            bossStats.increaseCurrentHealth(skillEffect * -1);
            knightStats.addExperience(skillEffect / 10);
            return "You did " + skill.getName() + " to the boss for " + skillEffect + " damage";
        }

        return "You don't have enough mana so you did nothing";

    }

    /**
     * Function to edit the knight's health due to boss' attack.
     * @return returns a log of the boss' attack.
     */
    public String bossAttack() {
        updateCheck();

        int damage = 10 * bossStats.getLevel() / 2 + random.nextInt(3);
        int absorbedDamage = knightService.knightDefend() / 4;
        knightStats.changeHealth(Math.max((damage - absorbedDamage), 0) * -1);

        return "The boss dealt " + damage + " to you";
    }
}
