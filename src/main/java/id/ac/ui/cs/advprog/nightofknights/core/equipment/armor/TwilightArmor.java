package id.ac.ui.cs.advprog.nightofknights.core.equipment.armor;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class TwilightArmor extends Equipment {
    /** Concrete class of armor TwilightArmor.*/
    public TwilightArmor() {
        this.name = "Twilight Armor";
        this.desc = "This is Twilight Armor";
        this.value = 11;
    }
}
