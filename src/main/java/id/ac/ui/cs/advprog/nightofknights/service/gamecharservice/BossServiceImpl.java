package id.ac.ui.cs.advprog.nightofknights.service.gamecharservice;

import id.ac.ui.cs.advprog.nightofknights.core.gamechar.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import org.springframework.stereotype.Service;

@Service
public class BossServiceImpl implements BossService {
    private GameChar boss;

    public BossServiceImpl() {
        boss = Boss.getInstance();
    }

    public GameChar getBoss() {
        return this.boss;
    }

    public Stats getBossStats() {
        return this.boss.getCharStats();
    }

    public void setBossStats(Stats charStats) {
        this.boss.setCharStats(charStats);
    }

    public void resetBossInstance() {
        Boss.deleteInstance();
        this.boss = Boss.getInstance();
    }

    public void resetBossCharStats() {
        this.boss.resetCharStats();
    }

    /**
     * A function that checks if the boss currently has 0 or less health.
     * @return boolean, true if dead, false if alive.
     */
    public boolean isBossDead() {
        Stats bossStats = getBossStats();

        if (bossStats.getCurrentHealth() <= 0) {
            return true;
        }

        return false;
    }
}
