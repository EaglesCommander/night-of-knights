package id.ac.ui.cs.advprog.nightofknights.core.equipment;

import org.springframework.stereotype.Component;

@Component
public class Equipment {
    protected String name;
    protected String desc;
    protected int value;
    protected int timesUpgraded = 0;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return desc;
    }

    public void upgrade() {
        timesUpgraded += 1;
    }

    public void upValue(int upgradeValue) {
        value += upgradeValue;
    }

    public int getTimesUpgraded() {
        return timesUpgraded;
    }
}
