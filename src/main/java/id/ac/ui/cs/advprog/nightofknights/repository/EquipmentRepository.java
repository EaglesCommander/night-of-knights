package id.ac.ui.cs.advprog.nightofknights.repository;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.EquipmentsFactory;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

@Repository
public class EquipmentRepository {
    EquipmentsFactory equipmentsFactory = new EquipmentsFactory();

    public Equipment[] createCharacter(String type) {
        return equipmentsFactory.createCharater(type);
    }

    public Equipment[] createStorage(String type) {
        return equipmentsFactory.createStorage(type);
    }
}
