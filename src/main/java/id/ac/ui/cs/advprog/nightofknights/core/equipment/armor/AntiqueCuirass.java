package id.ac.ui.cs.advprog.nightofknights.core.equipment.armor;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class AntiqueCuirass extends Equipment {
    /** Concrete class of armor AntiqueCuirass.*/
    public AntiqueCuirass() {
        this.name = "AntiqueCuirass";
        this.desc = "This is AntiqueCuirass";
        this.value = 15;
    }
}
