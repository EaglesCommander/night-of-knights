package id.ac.ui.cs.advprog.nightofknights.core.equipment.skill;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class PhantomSteed extends Equipment {
    /** Concrete class of skill PhantomSteed.*/
    public PhantomSteed() {
        this.name = "Phantom Steed";
        this.desc = "This is Phantom Steed";
        this.value = 550;
    }
}
