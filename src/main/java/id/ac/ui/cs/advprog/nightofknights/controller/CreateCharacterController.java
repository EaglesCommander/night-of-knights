package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.service.equipmentservice.EquipmentService;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.BossService;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/create")
public class CreateCharacterController {

    @Autowired
    private final EquipmentService equipmentService;

    @Autowired
    private final KnightService knightService;
    private final BossService bossService;

    /**
     * Constructor for battlecontroller.
     * @param knightService is for connecting with knight.
     * @param equipmentService is for connecting with equipment factory.
     */
    public CreateCharacterController(
            EquipmentService equipmentService,KnightService knightService,
            BossService bossService) {
        this.equipmentService = equipmentService;
        this.knightService = knightService;
        this.bossService = bossService;
    }

    /**
     * Function for accessing the battle page naturally.
     * @param model is the models for mvc.
     * @return returns a view.
     */
    @GetMapping(path = "/")
    public String createCharacterHome(Model model) {
        return "create-character";
    }

    /**
     * Function that is called after clicking the create character button.
     * @param knightName is the character name.
     * @param knightType is the character type.
     * @return returns a view.
     */
    @RequestMapping(path = "/produce",method = RequestMethod.POST)
    public String produceKnight(@RequestParam("knightName") String knightName,
                                @RequestParam("knightType") String knightType) {
        equipmentService.createChar(knightName,knightType);
        bossService.resetBossCharStats();
        return "redirect:/menu/";
    }
}
