package id.ac.ui.cs.advprog.nightofknights.core.enhancer;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import java.util.Random;

public class DarkUpgrade extends Equipment {

    Equipment eq;
    int upgradeValue;

    /**
     * Upgrade equipment with Dark.
     * @param eq = Weapon / Armor / Skill.
     */
    public DarkUpgrade(Equipment eq) {
        this.eq = eq;
        this.upgradeValue = getNumber();
        this.eq.upgrade();
        this.eq.upValue(this.upgradeValue);
    }

    public int getNumber() {
        return new Random().nextInt(21);
    }

    @Override
    public String getName() {
        return this.eq.getName();
    }

    @Override
    public int getValue() {
        return this.eq.getValue();
    }

    @Override
    public String getDescription() {
        return eq.getDescription() + ", upgraded "
                + eq.getTimesUpgraded() + " times.";
    }

}
