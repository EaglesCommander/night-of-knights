package id.ac.ui.cs.advprog.nightofknights.service.gamecharservice;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;
import id.ac.ui.cs.advprog.nightofknights.core.gamechar.GameChar;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import java.util.HashMap;
import java.util.Map;

public interface KnightService {
    public GameChar getKnight();

    public int knightAttack();

    public int knightSkill();

    public int knightDefend();

    public void setKnightWeapon(Equipment weapon);

    public void setKnightSkill(Equipment skill);

    public void setKnightArmor(Equipment armor);

    public Equipment getKnightWeapon();

    public Equipment getKnightSkill();

    public Equipment getKnightArmor();

    public Stats getKnightCharStats();

    public String getKnightName();

    public String getKnightClass();

    public void createKnight(String name, String knightClass,
                             Equipment weapon, Equipment skill, Equipment armor);

    public void resetKnightInstance();

    public boolean isKnightDead();

    public Map<String, Equipment> getKnightStorage();
}
