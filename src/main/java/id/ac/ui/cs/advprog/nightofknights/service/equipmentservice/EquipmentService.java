package id.ac.ui.cs.advprog.nightofknights.service.equipmentservice;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import java.util.ArrayList;


public interface EquipmentService {
    public Equipment[] createCharEquipment(String type);

    public Equipment[] createCharStorage(String type);

    public void createChar(String knightName, String knightType);
}
