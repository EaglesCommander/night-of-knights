package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.service.battleservice.BattleServiceImpl;
import java.util.function.Supplier;

public class KnightAttackSupplier implements Supplier<String> {

    private BattleServiceImpl battleService;

    public KnightAttackSupplier(BattleServiceImpl battleService) {
        this.battleService = battleService;
    }

    @Override
    public String get() {
        return battleService.knightAttack();
    }
}