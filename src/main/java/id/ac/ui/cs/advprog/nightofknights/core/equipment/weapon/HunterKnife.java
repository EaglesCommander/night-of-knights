package id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;

public class HunterKnife extends Equipment {
    /** Concrete class of weapon HunterKnife.*/
    public HunterKnife() {
        this.name = "Hunter Knife";
        this.desc = "This is Hunter Knife";
        this.value = 75;
    }
}
