package id.ac.ui.cs.advprog.nightofknights.controller;

import id.ac.ui.cs.advprog.nightofknights.feign.HistoryFeign;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/")
public class HistoryController {

    private final HistoryFeign historyFeign;

    public HistoryController(HistoryFeign historyFeign) {
        this.historyFeign = historyFeign;
    }

    /**
     * A function to load and contextualize history-service json.
     * @param model as a model to be used in the website.
     * @return a string of the view.
     */
    @GetMapping("/history/")
    public String getHistory(Model model) {
        String historyString = historyFeign.getHistory();

        String[] historyEntries = historyString.split("#");
        ArrayList<String[]> history = new ArrayList<String[]>();

        for (String entry : historyEntries) {
            String[] splitEntry = entry.split(",");
            history.add(splitEntry);
        }

        model.addAttribute("history", history);

        return "history";
    }
}