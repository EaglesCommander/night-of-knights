package id.ac.ui.cs.advprog.nightofknights.core.equipment;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.AntiqueCuirass;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.BruteForceBreastplate;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.TwilightArmor;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.Decimate;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.PhantomSteed;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.VioletRequiem;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.Dagger;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.HunterKnife;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.MysteryCodex;

public class EquipmentsFactory {
    /** Function to create selected knight equipment.
    *
    * @param type of the knight.
    */
    public Equipment[] createCharater(String type) {
        Equipment[] equipments = new Equipment[3];
        switch (type) {
          case "Magician": {
              equipments[0] = new MysteryCodex();
              equipments[1] = new VioletRequiem();
              equipments[2] = new TwilightArmor();
              break;
          }
          case "Knight": {
              equipments[0] = new Dagger();
              equipments[1] = new PhantomSteed();
              equipments[2] = new AntiqueCuirass();
              break;
          }

          case "Assassin": {
              equipments[0] = new HunterKnife();
              equipments[1] = new Decimate();
              equipments[2] = new BruteForceBreastplate();
              break;
          }
          default:
              break;
        }
        return equipments;
    }

    /**
     * Function to create selected knight's storage.
     * @param type of the knight
     * @return an array of equipments.
     */
    public Equipment[] createStorage(String type) {
        Equipment[] equipments = new Equipment[3];
        switch (type) {
          case "Magician": {
              equipments[0] = new Dagger();
              equipments[1] = new PhantomSteed();
              equipments[2] = new AntiqueCuirass();
              break;
          }
          case "Knight": {
              equipments[0] = new HunterKnife();
              equipments[1] = new Decimate();
              equipments[2] = new BruteForceBreastplate();
              break;
          }

          case "Assassin": {
              equipments[0] = new MysteryCodex();
              equipments[1] = new VioletRequiem();
              equipments[2] = new TwilightArmor();
              break;
          }
          default:
              break;
        }
        return equipments;
    }
}
