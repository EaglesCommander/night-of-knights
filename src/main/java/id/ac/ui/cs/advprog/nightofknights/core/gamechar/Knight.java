package id.ac.ui.cs.advprog.nightofknights.core.gamechar;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import java.util.HashMap;
import java.util.Map;

public class Knight implements GameChar {
    private Stats charStats;
    public boolean gameOver = false;

    private String name;
    private String knightClass;

    private Equipment weapon;
    private Equipment skill;
    private Equipment armor;

    private Map<String, Equipment> storage;

    private static Knight instance;

    private Knight() {
        this.charStats = new KnightStats();
        this.storage = new HashMap<>();
        this.name = null;
        this.knightClass = null;
        this.weapon = null;
        this.skill = null;
        this.armor = null;
    }

    /**
     * Singleton method to return the instance of Knight or make a new one.
     * @return returns the instance of knight.
     */
    public static Knight getInstance() {
        if (instance == null) {
            instance = new Knight();
        }

        return instance;
    }

    /**
     * Function to decorate the singleton instance with needed objects, previously null.
     * @param name name of the knight.
     * @param knightClass class of the knight.
     * @param weapon weapon of the knight.
     * @param skill skill of the knight.
     * @param armor armor of the knight.
     */
    public void createKnight(String name, String knightClass,
                             Equipment weapon, Equipment skill, Equipment armor) {
        this.name = name;
        this.knightClass = knightClass;
        this.weapon = weapon;
        this.skill = skill;
        this.armor = armor;
        this.charStats = new KnightStats();
        this.gameOver = false;
    }

    /**
     * A function to get the value of the equipped weapon.
     * @return an integer of the weapon value or 0 if weapon is null.
     */
    public int attack() {
        try {
            return this.weapon.getValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * A function to get the value of the equipped skill.
     * @return an integer of the skill value or 0 if skill is null.
     */
    public int skill() {
        try {
            return this.skill.getValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * A function to get the value of the equipped armor.
     * @return an integer of the armor value or 0 if armor is null.
     */
    public int defend() {
        try {
            return this.armor.getValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public void setWeapon(Equipment weapon) {
        this.weapon = weapon;
    }

    public void setSkill(Equipment skill) {
        this.skill = skill;
    }

    public void setArmor(Equipment armor) {
        this.armor = armor;
    }

    public void setCharStats(Stats charStats) {
        this.charStats = charStats;
    }

    public void resetCharStats() {
        this.charStats = new KnightStats();
    }

    public Equipment getWeapon() {
        return this.weapon;
    }

    public Equipment getSkill() {
        return this.skill;
    }

    public Equipment getArmor() {
        return this.armor;
    }

    public Stats getCharStats() {
        return this.charStats;
    }

    public String getName() {
        return this.name;
    }

    public String getKnightClass() {
        return this.knightClass;
    }

    public Map<String, Equipment> getStorage() {
        return this.storage;
    }

    public static void deleteInstance() {
        Knight.instance = null;
    }
}
