package id.ac.ui.cs.advprog.nightofknights.core.stats;

public class KnightStats implements Stats {
    private int maxHealth;
    private int currentHealth;
    private int maxMana;
    private int currentMana;
    private int experience;

    /**
     * constructor for KnightStats. Provides a default 0 for experience and 100 for anything else.
     */
    public KnightStats() {
        this.maxHealth = 100;
        this.currentHealth = 100;
        this.maxMana = 100;
        this.currentMana = 100;
        this.experience = 0;
    }

    public int getMaxHealth() {
        return this.maxHealth;
    }

    public int getCurrentHealth() {
        return this.currentHealth;
    }

    public int getMaxMana() {
        return this.maxMana;
    }

    public int getCurrentMana() {
        return this.currentMana;
    }

    public int getExperience() {
        return this.experience;
    }

    public void setMaxHealth(int amount) {
        this.maxHealth = amount;
    }

    public void setMaxMana(int amount) {
        this.maxMana = amount;
    }

    /**
     * A function to increase or decrease the health of the knight.
     * Amount cannot overflow.
     * @param amount amount to be increased or decreased.
     */
    public void changeHealth(int amount) {
        if (this.currentHealth <= 0) {
            return;
        }

        this.currentHealth += amount;
        if (this.getCurrentHealth() > this.getMaxHealth()) {
            this.changeHealth(this.getMaxHealth() - this.getCurrentHealth());
        }
    }

    /**
     * A function to increase or decrease the mana of the knight.
     * Amount cannot overflow.
     * @param amount amount to be increased or decreased.
     */
    public void changeMana(int amount) {
        this.currentMana += amount;
        if (this.getCurrentMana() > this.getMaxMana()) {
            this.changeMana(this.getMaxMana() - this.getCurrentMana());
        }
    }

    public void addExperience(int amount) {
        this.experience += amount;
    }
}
