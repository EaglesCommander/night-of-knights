package id.ac.ui.cs.advprog.nightofknights.service.enhancerservice;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;

public interface EnhancerService {

    public Equipment equipmentLightUpgrade(Equipment eq);

    public Equipment equipmentDarkUpgrade(Equipment eq);

    public String equipmentGetName();

    public int equipmentGetValue();

    public String equipmentGetDesc();

}
