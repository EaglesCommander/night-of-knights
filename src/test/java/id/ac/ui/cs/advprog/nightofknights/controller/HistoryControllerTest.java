package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advprog.nightofknights.feign.HistoryFeign;
import java.util.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = HistoryController.class)
@ExtendWith(MockitoExtension.class)
public class HistoryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HistoryFeign historyFeign;

    @Test
    public void testHistoryScreen() throws Exception {
        when(historyFeign.getHistory())
                .thenReturn("a,b,c#d,e,f");

        mockMvc.perform(get("/history/"))
                .andExpect(status().isOk());
    }
}
