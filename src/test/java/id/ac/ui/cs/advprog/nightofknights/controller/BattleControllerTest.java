package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advprog.nightofknights.core.gamechar.Knight;
import id.ac.ui.cs.advprog.nightofknights.core.stats.BossStats;
import id.ac.ui.cs.advprog.nightofknights.feign.HistoryFeign;
import id.ac.ui.cs.advprog.nightofknights.service.battleservice.BattleServiceImpl;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.BossServiceImpl;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

@WebMvcTest(controllers = BattleController.class)
@ExtendWith(MockitoExtension.class)
public class BattleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private BattleServiceImpl battleService;

    @MockBean
    private KnightServiceImpl knightService;

    @MockBean
    private BossServiceImpl bossService;

    @MockBean
    private HistoryFeign historyFeign;

    @Test
    public void testAccessBossScreen() throws Exception {
        mockMvc.perform(get("/boss-battle/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAttackButton() throws Exception {
        mockMvc.perform(get("/boss-battle/attack/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAttackButtonException() throws Exception {
        when(battleService.knightAttack())
                .thenThrow(MockitoException.class);

        mockMvc.perform(get("/boss-battle/attack/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDefendButton() throws Exception {
        mockMvc.perform(get("/boss-battle/defend/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDefendButtonException() throws Exception {
        when(battleService.knightDefend())
                .thenThrow(MockitoException.class);

        mockMvc.perform(get("/boss-battle/defend/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSkillButton() throws Exception {
        mockMvc.perform(get("/boss-battle/skill/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSkillButtonException() throws Exception {
        when(battleService.knightSkill())
                .thenThrow(MockitoException.class);

        mockMvc.perform(get("/boss-battle/skill/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testVictoryPage() throws Exception {
        mockMvc.perform(get("/boss-battle/victory/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDefeatPage() throws Exception {
        mockMvc.perform(get("/boss-battle/defeat/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testKnightStatus() throws Exception {
        mockMvc.perform(get("/boss-battle/knight-status/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testBossStatus() throws Exception {
        mockMvc.perform(get("/boss-battle/boss-status/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testRecordNothing() throws Exception {
        Knight knight = Knight.getInstance();
        BossStats bossStats = new BossStats();

        knight.gameOver = true;

        when(knightService.getKnight())
                .thenReturn(knight);

        when(bossService.getBossStats())
                .thenReturn(bossStats);

        when(knightService.isKnightDead())
                .thenReturn(false);

        mockMvc.perform(get("/record/"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testRecordVictory() throws Exception {
        Knight knight = Knight.getInstance();
        BossStats bossStats = new BossStats();

        knight.gameOver = false;

        when(knightService.getKnight())
                .thenReturn(knight);

        when(bossService.getBossStats())
                .thenReturn(bossStats);

        when(knightService.isKnightDead())
                .thenReturn(false);

        mockMvc.perform(get("/record/"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testRecordDefeat() throws Exception {
        Knight knight = Knight.getInstance();
        BossStats bossStats = new BossStats();

        knight.gameOver = false;

        when(knightService.getKnight())
                .thenReturn(knight);

        when(bossService.getBossStats())
                .thenReturn(bossStats);

        when(knightService.isKnightDead())
                .thenReturn(true);

        mockMvc.perform(get("/record/"))
                .andExpect(status().is3xxRedirection());
    }
}
