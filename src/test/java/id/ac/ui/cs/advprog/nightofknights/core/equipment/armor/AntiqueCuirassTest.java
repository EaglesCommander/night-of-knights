package id.ac.ui.cs.advprog.nightofknights.core.equipment.armor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AntiqueCuirassTest {

    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new AntiqueCuirass();
    }

    @Test
    public void testGetValue() {
        assertEquals(15, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("AntiqueCuirass", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is AntiqueCuirass", equipment.getDescription());
    }
}
