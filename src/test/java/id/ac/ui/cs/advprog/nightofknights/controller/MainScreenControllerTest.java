package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = MainScreenController.class)
public class MainScreenControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testMainScreen() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk());
    }
}
