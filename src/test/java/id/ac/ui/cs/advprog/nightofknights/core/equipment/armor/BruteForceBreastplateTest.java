package id.ac.ui.cs.advprog.nightofknights.core.equipment.armor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BruteForceBreastplateTest {

    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new BruteForceBreastplate();
    }

    @Test
    public void testGetValue() {
        assertEquals(5, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Brute Force Breastplate", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Brute Force Breastplate", equipment.getDescription());
    }
}
