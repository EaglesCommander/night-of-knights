package id.ac.ui.cs.advprog.nightofknights.core.stats;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class KnightStatsTest {
    KnightStats knightStats;

    @BeforeEach
    public void setUp() {
        knightStats = new KnightStats();
    }

    @Test
    public void testGetMaxHealth() {
        assertEquals(100, knightStats.getMaxHealth());
    }

    @Test
    public void testGetCurrentHealth() {
        assertEquals(100, knightStats.getCurrentHealth());
    }

    @Test
    public void testGetMaxMana() {
        assertEquals(100, knightStats.getMaxMana());
    }

    @Test
    public void testGetCurrentMana() {
        assertEquals(100, knightStats.getCurrentMana());
    }

    @Test
    public void testGetExperience() {
        assertEquals(0, knightStats.getExperience());
    }

    @Test
    public void testSetMaxHealth() {
        knightStats.setMaxHealth(200);
        assertEquals(200, knightStats.getMaxHealth());
    }

    @Test
    public void testSetMaxMana() {
        knightStats.setMaxMana(200);
        assertEquals(200, knightStats.getMaxMana());
    }

    @Test
    public void testChangeHealth() {
        knightStats.changeHealth(-10);
        assertEquals(90, knightStats.getCurrentHealth());
    }

    @Test
    public void testChangeHealthMoreThanMax() {
        knightStats.changeHealth(100);
        assertEquals(100, knightStats.getCurrentHealth());
    }

    @Test
    public void testChangeHealthDead() {
        knightStats.changeHealth(-200);
        knightStats.changeHealth(1000);
        assertEquals(-100, knightStats.getCurrentHealth());
    }

    @Test
    public void testChangeMana() {
        knightStats.changeMana(-10);
        assertEquals(90, knightStats.getCurrentMana());
    }

    @Test
    public void testChangeManaMoreThanMax() {
        knightStats.changeMana(100);
        assertEquals(100, knightStats.getCurrentMana());
    }

    @Test
    public void testAddExperience() {
        knightStats.addExperience(100);
        assertEquals(100, knightStats.getExperience());
    }
}
