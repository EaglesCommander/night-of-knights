package id.ac.ui.cs.advprog.nightofknights.service.gamecharservice;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.*;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.*;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.*;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class KnightServiceImplTest {
    KnightServiceImpl knightService;

    /**
     * Function for repeating the creation of Knight before each test.
     */
    @BeforeEach
    public void setUp() {
        knightService = new KnightServiceImpl();
        knightService.createKnight("Nico", "Idol", new MysteryCodex(),
                new VioletRequiem(), new BruteForceBreastplate());
    }

    @Test
    public void testGetKnight() {
        assertNotNull(knightService.getKnight());
    }

    @Test
    public void testKnightAttack() {
        assertNotNull(knightService.knightAttack());
    }

    @Test
    public void testKnightDefend() {
        assertNotNull(knightService.knightDefend());
    }

    @Test
    public void testKnightSkill() {
        assertNotNull(knightService.knightSkill());
    }

    @Test
    public void testSetGetKnightWeapon() {
        Equipment weapon1 = new Dagger();
        knightService.setKnightWeapon(weapon1);
        assertEquals(weapon1, knightService.getKnightWeapon());
    }

    @Test
    public void testSetGetKnightArmor() {
        Equipment armor1 = new TwilightArmor();
        knightService.setKnightArmor(armor1);
        assertEquals(armor1, knightService.getKnightArmor());
    }

    @Test
    public void testSetGetKnightSkill() {
        Equipment skill1 = new Decimate();
        knightService.setKnightSkill(skill1);
        assertEquals(skill1, knightService.getKnightSkill());
    }

    @Test
    public void testSetGetKnightCharStats() {
        Stats charStats = new KnightStats();
        knightService.setKnightCharStats(charStats);
        assertEquals(charStats, knightService.getKnightCharStats());
    }

    @Test
    public void testGetKnightName() {
        assertEquals("Nico", knightService.getKnightName());
    }

    @Test
    public void testGetKnightClass() {
        assertEquals("Idol", knightService.getKnightClass());
    }

    @Test
    public void testDeleteInstance() {
        knightService.resetKnightInstance();
        assertNull(knightService.getKnightName());
    }

    @Test
    public void testIsKnightDead() {
        assertFalse(knightService.isKnightDead());
        KnightStats charStats = new KnightStats();
        charStats.changeHealth(-1 * charStats.getCurrentHealth());
        knightService.setKnightCharStats(charStats);
        assertTrue(knightService.isKnightDead());
    }

    @Test
    public void testGetStorage() {
        assertNotNull(knightService.getKnightStorage());
    }

}
