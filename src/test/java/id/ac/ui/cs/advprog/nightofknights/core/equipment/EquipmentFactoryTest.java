package id.ac.ui.cs.advprog.nightofknights.core.equipment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EquipmentFactoryTest {

    private EquipmentsFactory eq;

    @BeforeEach
    public void setUp() {
        eq = new EquipmentsFactory();
    }

    @Test
    public void testCreateMagicianEquipments() {
        Equipment[] charaterEquipments = eq.createCharater("Magician");
        assertNotNull(charaterEquipments);
        assertEquals("Mystery Codex", charaterEquipments[0].getName());
        assertEquals("Violet Requiem", charaterEquipments[1].getName());
        assertEquals("Twilight Armor", charaterEquipments[2].getName());
        Equipment[] charaterStorage = eq.createStorage("Magician");
        assertNotNull(charaterStorage);
        assertEquals("Dagger", charaterStorage[0].getName());
        assertEquals("Phantom Steed", charaterStorage[1].getName());
        assertEquals("AntiqueCuirass", charaterStorage[2].getName());

    }

    @Test
    public void testCreateKnightEquipments() {
        Equipment[] charaterEquipments = eq.createCharater("Knight");
        assertNotNull(charaterEquipments);
        assertEquals("Dagger", charaterEquipments[0].getName());
        assertEquals("Phantom Steed", charaterEquipments[1].getName());
        assertEquals("AntiqueCuirass", charaterEquipments[2].getName());
        Equipment[] charaterStorage = eq.createStorage("Knight");
        assertNotNull(charaterStorage);
        assertEquals("Hunter Knife", charaterStorage[0].getName());
        assertEquals("Decimate", charaterStorage[1].getName());
        assertEquals("Brute Force Breastplate", charaterStorage[2].getName());
    }

    @Test
    public void testCreateAssassinEquipments() {
        Equipment[] charaterEquipments = eq.createCharater("Assassin");
        assertNotNull(charaterEquipments);
        assertEquals("Hunter Knife", charaterEquipments[0].getName());
        assertEquals("Decimate", charaterEquipments[1].getName());
        assertEquals("Brute Force Breastplate", charaterEquipments[2].getName());
        Equipment[] charaterStorage = eq.createStorage("Assassin");
        assertNotNull(charaterStorage);
        assertEquals("Mystery Codex", charaterStorage[0].getName());
        assertEquals("Violet Requiem", charaterStorage[1].getName());
        assertEquals("Twilight Armor", charaterStorage[2].getName());
    }

    @Test
    public void testCreateNullEquipments() {
        Equipment[] charaterEquipments = eq.createCharater("Null");
        assertNotNull(charaterEquipments);
        assertEquals(null, charaterEquipments[0]);
        assertEquals(null, charaterEquipments[1]);
        assertEquals(null, charaterEquipments[2]);
    }
}