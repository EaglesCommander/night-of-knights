package id.ac.ui.cs.advprog.nightofknights;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class NightOfKnightsApplicationTest {
    @Test
    void contextLoads() {
    }

    @Test
    public void main() {
        NightOfKnightsApplication.main(new String[]{});
    }
}
