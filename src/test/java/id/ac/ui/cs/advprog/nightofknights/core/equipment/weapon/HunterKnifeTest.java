package id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HunterKnifeTest {

    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new HunterKnife();
    }

    @Test
    public void testGetValue() {
        assertEquals(75, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Hunter Knife", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Hunter Knife", equipment.getDescription());
    }
}
