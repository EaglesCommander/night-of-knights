package id.ac.ui.cs.advprog.nightofknights.core.equipment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class EquipmentTest {

    private Equipment eq;

    @BeforeEach
    public void setUp() {
        eq = Mockito.mock(Equipment.class);
    }

    @Test
    public void testGetName() {
        Mockito.doCallRealMethod().when(eq).getName();
        String eqName = eq.getName();
        assertEquals(null, eqName);
    }

    @Test
    public void testGetValue() {
        Mockito.doCallRealMethod().when(eq).getValue();
        int eqValue = eq.getValue();
        assertEquals(0, eqValue);
    }

    @Test
    public void testGetDescription() {
        Mockito.doCallRealMethod().when(eq).getDescription();
        String eqDesc = eq.getDescription();
        assertEquals(null, eqDesc);
    }

    @Test
    public void testUpgrade() {
        Mockito.doCallRealMethod().when(eq).upgrade();
        Mockito.doCallRealMethod().when(eq).getTimesUpgraded();
        eq.upgrade();
        assertEquals(1, eq.getTimesUpgraded());
    }

    @Test
    public void testUpValue() {
        int tmp = 5;
        Mockito.doCallRealMethod().when(eq).upValue(tmp);
        Mockito.doCallRealMethod().when(eq).getValue();
        eq.upValue(tmp);
        assertEquals(5, eq.getValue());
    }

    @Test
    public void getTimesUpgraded() {
        Mockito.doCallRealMethod().when(eq).getTimesUpgraded();
        eq.getTimesUpgraded();
        assertEquals(0, eq.getTimesUpgraded());
    }
}