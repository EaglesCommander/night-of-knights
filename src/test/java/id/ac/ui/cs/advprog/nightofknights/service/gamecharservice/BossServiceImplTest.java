package id.ac.ui.cs.advprog.nightofknights.service.gamecharservice;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.nightofknights.core.gamechar.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.BossStats;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BossServiceImplTest {
    BossServiceImpl bossService;

    @BeforeEach
    public void setUp() {
        bossService = new BossServiceImpl();
    }

    @Test
    public void testGetInstance() {
        assertNotNull(bossService.getBoss());
    }

    @Test
    public void testSetGetBossStats() {
        BossStats bossStats = new BossStats();
        bossService.setBossStats(bossStats);
        assertEquals(bossStats, bossService.getBossStats());
    }

    @Test
    public void testIsBossDead() {
        assertFalse(bossService.isBossDead());
        BossStats bossStats = new BossStats();
        bossStats.increaseCurrentHealth(bossStats.getCurrentHealth() * -1);
        bossService.setBossStats(bossStats);
        assertTrue(bossService.isBossDead());
    }

    @Test
    public void testResetBossCharStats() {
        BossStats bossStats = (BossStats) bossService.getBossStats();
        bossService.resetBossCharStats();
        assertNotEquals(bossStats, bossService.getBossStats());
    }

    @Test
    public void testResetBossInstance() {
        Boss boss = (Boss) bossService.getBoss();
        bossService.resetBossInstance();
        assertNotEquals(boss, bossService.getBoss());
    }
}
