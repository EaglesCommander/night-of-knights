package id.ac.ui.cs.advprog.nightofknights.core.gamechar;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.*;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.*;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class KnightTest {

    private Class<?> knightClass;
    private Knight knight;

    /**
     * Function for repeating the instantiation of a knight before each test.
     * @throws Exception if the Class forName collects up garbage.
     */
    @BeforeEach
    public void setUp() throws Exception {
        knight = Knight.getInstance();
        knightClass = Class.forName(Knight.class.getName());
        knight.createKnight("Nico", "Idol", new Dagger(), new Decimate(), new TwilightArmor());
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(knightClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstance() {
        assertEquals(knight, Knight.getInstance());
        assertEquals(knight.getClass(), knightClass);
    }

    @Test
    public void testCreateKnight() {
        knight.createKnight("Nico", "Idol", new Dagger(), new Decimate(), new TwilightArmor());
        assertEquals(knight.getName(), "Nico");
    }

    @Test
    public void testAttackNotNull() {
        assertNotNull(knight.attack());
    }

    @Test
    public void testDefendNotNull() {
        assertNotNull(knight.defend());
    }

    @Test
    public void testSkillNotNull() {
        assertNotNull(knight.skill());
    }

    @Test
    public void testAttackZero() {
        knight.setWeapon(null);
        assertEquals(0, knight.attack());
    }

    @Test
    public void testDefendZero() {
        knight.setArmor(null);
        assertEquals(0, knight.defend());
    }

    @Test
    public void testSkillZero() {
        knight.setSkill(null);
        assertEquals(0, knight.skill());
    }

    @Test
    public void testGetWeapon() {
        assertNotNull(knight.getWeapon());
    }

    @Test
    public void testSetWeapon() {
        Equipment weapon2 = new HunterKnife();
        knight.setWeapon(weapon2);
        assertEquals(weapon2, knight.getWeapon());
    }

    @Test
    public void testGetArmor() {
        assertNotNull(knight.getArmor());
    }

    @Test
    public void testSetArmor() {
        Equipment armor2 = new AntiqueCuirass();
        knight.setArmor(armor2);
        assertEquals(armor2, knight.getArmor());
    }

    @Test
    public void testGetSkill() {
        assertNotNull(knight.getSkill());
    }

    @Test
    public void testSetSkill() {
        Equipment skill2 = new PhantomSteed();
        knight.setSkill(skill2);
        assertEquals(skill2, knight.getSkill());
    }

    @Test
    public void testGetCharStats() {
        assertNotNull(knight.getCharStats());
    }

    @Test
    public void testSetCharStats() {
        Stats stats2 = new KnightStats();
        knight.setCharStats(stats2);
        assertEquals(stats2, knight.getCharStats());
    }

    @Test
    public void testResetCharStats() {
        Stats stats2 = (KnightStats) knight.getCharStats();
        knight.resetCharStats();
        assertNotEquals(stats2, knight.getCharStats());
    }

    @Test
    public void testGetName() {
        assertEquals("Nico", knight.getName());
    }

    @Test
    public void testKnightClass() {
        assertEquals("Idol", knight.getKnightClass());
    }

    @Test
    public void testGetStorage() {
        assertNotNull(knight.getStorage());
    }

    @Test
    public void testDeleteInstance() {
        Knight.deleteInstance();
        assertNotEquals(knight, Knight.getInstance());
    }
}
