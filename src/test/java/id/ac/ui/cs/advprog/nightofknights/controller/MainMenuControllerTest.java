package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = MainMenuController.class)
public class MainMenuControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testMenuScreen() throws Exception {
        mockMvc.perform(get("/menu/"))
                .andExpect(status().isOk());
    }
}
