package id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MysteryCodexTest {

    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new MysteryCodex();
    }

    @Test
    public void testGetValue() {
        assertEquals(60, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Mystery Codex", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Mystery Codex", equipment.getDescription());
    }
}
