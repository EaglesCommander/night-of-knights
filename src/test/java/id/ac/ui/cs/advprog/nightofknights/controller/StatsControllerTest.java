package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advprog.nightofknights.controller.StatsController;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = StatsController.class)
public class StatsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private KnightServiceImpl knightService;

    @Test
    public void testAccessStatsScreen() throws Exception {
        mockMvc.perform(get("/stats/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testChangeEquipment() throws Exception {
        mockMvc.perform(get("/stats/change/"))
                .andExpect(redirectedUrl("/stats/"));
    }
}
