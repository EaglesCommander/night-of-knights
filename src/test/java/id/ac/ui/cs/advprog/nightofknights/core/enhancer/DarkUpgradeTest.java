package id.ac.ui.cs.advprog.nightofknights.core.enhancer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.Dagger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class DarkUpgradeTest {

    private DarkUpgrade darkUpgrade;

    @BeforeEach
    public void setUp() {
        darkUpgrade = new DarkUpgrade(new Dagger());
    }

    @Test
    public void testGetNumber() {
        int val = darkUpgrade.getNumber();
        assertTrue(val >= 0 && val <= 20);
    }

    @Test
    public void testUpgrade() {
        assertEquals(0, darkUpgrade.getTimesUpgraded());
        darkUpgrade.upgrade();
        assertEquals(1, darkUpgrade.getTimesUpgraded());
    }

    @Test
    public void testGetName() {
        assertEquals("Dagger", darkUpgrade.getName());
    }

    @Test
    public void testGetValue() {
        assertTrue(darkUpgrade.getValue() >= 65 && darkUpgrade.getValue() <= 85);
    }

    @Test
    public void testGetDescription() {
        darkUpgrade.upgrade();
        assertEquals("This is Dagger, upgraded 1 times.", darkUpgrade.getDescription());
    }


}
