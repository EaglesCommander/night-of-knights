package id.ac.ui.cs.advprog.nightofknights.core.enhancer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.TwilightArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LightUpgradeTest {

    private LightUpgrade lightUpgrade;

    @BeforeEach
    public void setUp() {
        lightUpgrade = new LightUpgrade(new TwilightArmor());
    }

    @Test
    public void testGetNumber() {
        int val = lightUpgrade.getNumber();
        assertTrue(val == 10);
    }

    @Test
    public void testUpgrade() {
        assertEquals(0, lightUpgrade.getTimesUpgraded());
        lightUpgrade.upgrade();
        assertEquals(1, lightUpgrade.getTimesUpgraded());
    }

    @Test
    public void testGetName() {
        assertEquals("Twilight Armor", lightUpgrade.getName());
    }

    @Test
    public void testGetValue() {
        assertTrue(lightUpgrade.getValue() == 21);
    }

    @Test
    public void testGetDescription() {
        lightUpgrade.upgrade();
        assertEquals("This is Twilight Armor, upgraded 1 times.", lightUpgrade.getDescription());
    }
}
