package id.ac.ui.cs.advprog.nightofknights.service.enhancerservice;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.Dagger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EnhancerServiceImplTest {
    EnhancerServiceImpl enhancerService;

    @BeforeEach
    public void setUp() {
        enhancerService = new EnhancerServiceImpl(new Dagger());
    }

    @Test
    public void testLightUpgrade() {
        assertNotNull(enhancerService.equipmentLightUpgrade(new Dagger()));
    }

    @Test
    public void testDarkUpgrade() {
        assertNotNull(enhancerService.equipmentDarkUpgrade(new Dagger()));
    }

    @Test
    public void testGetName() {
        assertEquals("Dagger", enhancerService.equipmentGetName());
    }

    @Test
    public void testGetValue() {
        assertTrue(enhancerService.equipmentGetValue() == 65);
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Dagger", enhancerService.equipmentGetDesc());
    }
}
