package id.ac.ui.cs.advprog.nightofknights.core.stats;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BossStatsTest {

    BossStats bossStats;

    @BeforeEach
    public void setUp() {
        bossStats = new BossStats();
    }

    @Test
    public void testGetMaxHealth() {
        assertTrue(bossStats.getMaxHealth() > 0);
    }

    @Test
    public void testGetCurrentHealth() {
        assertTrue(bossStats.getCurrentHealth() > 0);
    }

    @Test
    public void testGetLevel() {
        assertEquals(0, bossStats.getLevel());
    }

    @Test
    public void testIncreaseCurrentHealth() {
        int currentHealth = bossStats.getCurrentHealth();
        bossStats.increaseCurrentHealth(-10);
        assertEquals(currentHealth - 10, bossStats.getCurrentHealth());
    }

    @Test
    public void testIncreaseLevel() {
        bossStats.increaseLevel(25);
        assertEquals(25, bossStats.getLevel());
    }
}