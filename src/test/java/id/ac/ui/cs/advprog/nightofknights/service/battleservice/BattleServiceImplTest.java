package id.ac.ui.cs.advprog.nightofknights.service.battleservice;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.armor.AntiqueCuirass;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.skill.Decimate;
import id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon.Dagger;
import id.ac.ui.cs.advprog.nightofknights.core.gamechar.*;
import id.ac.ui.cs.advprog.nightofknights.core.stats.KnightStats;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BattleServiceImplTest {
    BattleServiceImpl battleService;
    Knight knight;
    KnightStats knightStats;

    /**
     * A set up for each test.
     */
    @BeforeEach
    public void setUp() {
        battleService = new BattleServiceImpl();
        knight = Knight.getInstance();
        knight.createKnight("Nico", "Magician", new Dagger(), new Decimate(), new AntiqueCuirass());
    }

    @Test
    public void testAccess() {
        String string1 = battleService.bossAttack();
        battleService.access();
        assertNotEquals(string1, battleService.bossAttack());
    }

    @Test
    public void testKnightAttack() {
        assertNotNull(battleService.knightAttack());
    }

    @Test
    public void testKnightDefend() {
        assertNotNull(battleService.knightDefend());
    }

    @Test
    public void testKnightSkillMoreThan30() {
        knightStats = (KnightStats) knight.getCharStats();
        knightStats.changeMana(100);
        assertNotNull(battleService.knightSkill());
    }

    @Test
    public void testKnightSkillLessThan30() {
        knightStats = (KnightStats) knight.getCharStats();
        knightStats.changeMana(-100);
        assertNotNull(battleService.knightSkill());
    }

    @Test
    public void testBossAttack() {
        assertNotNull(battleService.bossAttack());
    }
}
