package id.ac.ui.cs.advprog.nightofknights.core.equipment.armor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TwilightArmorTest {

    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new TwilightArmor();
    }

    @Test
    public void testGetValue() {
        assertEquals(11, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Twilight Armor", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Twilight Armor", equipment.getDescription());
    }
}
