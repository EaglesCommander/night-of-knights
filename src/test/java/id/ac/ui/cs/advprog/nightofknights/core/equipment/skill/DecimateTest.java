package id.ac.ui.cs.advprog.nightofknights.core.equipment.skill;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DecimateTest {
    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new Decimate();
    }

    @Test
    public void testGetValue() {
        assertEquals(850, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Decimate", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Decimate", equipment.getDescription());
    }
}
