package id.ac.ui.cs.advprog.nightofknights.core.equipment.weapon;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DaggerTest {

    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new Dagger();
    }

    @Test
    public void testGetValue() {
        assertEquals(65, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Dagger", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Dagger", equipment.getDescription());
    }
}
