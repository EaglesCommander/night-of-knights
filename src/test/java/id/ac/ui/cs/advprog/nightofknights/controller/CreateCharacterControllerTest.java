package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advprog.nightofknights.service.equipmentservice.EquipmentService;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.BossService;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightService;
import javax.swing.text.html.parser.Entity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@WebMvcTest(controllers = CreateCharacterController.class)
@ExtendWith(MockitoExtension.class)
public class CreateCharacterControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EquipmentService equipmentService;

    @MockBean
    private KnightService knightService;

    @MockBean
    private BossService bossService;

    @Test
    public void testCreateCharacterScreen() throws Exception {
        mockMvc.perform(get("/create/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testProduceKnight() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("knightName", "Budi");
        params.add("knightType", "Knight");

        mockMvc.perform(post("/create/produce").params(params))
                .andExpect(handler().methodName("produceKnight"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/menu/"));
    }


}
