package id.ac.ui.cs.advprog.nightofknights.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.ac.ui.cs.advprog.nightofknights.controller.EnhancerController;
import id.ac.ui.cs.advprog.nightofknights.core.stats.KnightStats;
import id.ac.ui.cs.advprog.nightofknights.service.enhancerservice.EnhancerServiceImpl;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = EnhancerController.class)
public class EnhancerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnhancerServiceImpl enhancerService;

    @MockBean
    private KnightServiceImpl knightService;

    @Test
    public void testAccessShopScreen() throws Exception {
        mockMvc.perform(get("/shop/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testWeaponDarkUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/weapon/dark/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testWeaponDarkUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/weapon/dark/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testWeaponLightUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/weapon/light/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testWeaponLightUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/weapon/light/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testArmorDarkUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/armor/dark/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testArmorDarkUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/armor/dark/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testArmorLightUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/armor/light/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testArmorLightUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/armor/light/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testSkillDarkUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/skill/dark/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testSkillDarkUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/skill/dark/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testSkillLightUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/skill/light/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testSkillLightUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/skill/light/"))
                .andExpect(redirectedUrl("/shop/"));
    }


    @Test
    public void testHealthUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/health/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testHealthUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/health/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testManaUpgrade1() throws Exception {
        KnightStats k = new KnightStats();
        k.addExperience(100);
        when(knightService.getKnightCharStats())
                .thenReturn(k);

        mockMvc.perform(get("/shop/upgrade/mana/"))
                .andExpect(redirectedUrl("/shop/"));
    }

    @Test
    public void testManaUpgrade2() throws Exception {
        mockMvc.perform(get("/shop/upgrade/mana/"))
                .andExpect(redirectedUrl("/shop/"));
    }
}
