package id.ac.ui.cs.advprog.nightofknights.core.equipment.skill;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PhantomSteedTest {
    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new PhantomSteed();
    }

    @Test
    public void testGetValue() {
        assertEquals(550, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Phantom Steed", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Phantom Steed", equipment.getDescription());
    }

}
