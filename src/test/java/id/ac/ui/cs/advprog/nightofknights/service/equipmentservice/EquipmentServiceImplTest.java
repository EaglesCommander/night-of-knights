package id.ac.ui.cs.advprog.nightofknights.service.equipmentservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightService;
import id.ac.ui.cs.advprog.nightofknights.service.gamecharservice.KnightServiceImpl;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

@ExtendWith(MockitoExtension.class)
public class EquipmentServiceImplTest {
    private EquipmentService equipmentService;

    @BeforeEach
    public void setUp() {
        equipmentService = new EquipmentServiceImpl();
    }

    @Test
    public void testIntialEquipmentServiceImpl() {
        assertNotNull(equipmentService);
    }

    @Test
    public void testMagicalEquipmentProduction() {
        Equipment[] charEquipment = equipmentService.createCharEquipment("Magician");
        assertNotNull(charEquipment);
        assertEquals("Mystery Codex",charEquipment[0].getName());
        assertEquals("Violet Requiem", charEquipment[1].getName());
        assertEquals("Twilight Armor", charEquipment[2].getName());
    }

    @Test
    public void testMagicalStorageProduction() {
        Equipment[] charStorage = equipmentService.createCharStorage("Magician");
        assertNotNull(charStorage);
        assertEquals("Dagger",charStorage[0].getName());
        assertEquals("Phantom Steed", charStorage[1].getName());
        assertEquals("AntiqueCuirass", charStorage[2].getName());
    }

    @Test
    public void testKnightEquipmentProduction() {
        Equipment[] charEquipment = equipmentService.createCharEquipment("Knight");
        assertNotNull(charEquipment);
        assertEquals("Dagger",charEquipment[0].getName());
        assertEquals("Phantom Steed", charEquipment[1].getName());
        assertEquals("AntiqueCuirass", charEquipment[2].getName());
    }

    @Test
    public void testKnightStorageProduction() {
        Equipment[] charStorage = equipmentService.createCharStorage("Knight");
        assertNotNull(charStorage);
        assertEquals("Hunter Knife",charStorage[0].getName());
        assertEquals("Decimate", charStorage[1].getName());
        assertEquals("Brute Force Breastplate", charStorage[2].getName());
    }

    @Test
    public void testAsssasinEquipmentProduction() {
        Equipment[] charEquipment = equipmentService.createCharEquipment("Assassin");
        assertNotNull(charEquipment);
        assertEquals("Hunter Knife",charEquipment[0].getName());
        assertEquals("Decimate", charEquipment[1].getName());
        assertEquals("Brute Force Breastplate", charEquipment[2].getName());
    }



    @Test
    public void testAsssasinStorageProduction() {
        Equipment[] charStorage = equipmentService.createCharStorage("Assassin");
        assertNotNull(charStorage);
        assertEquals("Mystery Codex",charStorage[0].getName());
        assertEquals("Violet Requiem", charStorage[1].getName());
        assertEquals("Twilight Armor", charStorage[2].getName());
    }
}
