package id.ac.ui.cs.advprog.nightofknights.core.equipment.skill;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.nightofknights.core.equipment.Equipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class VioletRequiemTest {
    Equipment equipment;

    @BeforeEach
    public void setUp() {
        equipment = new VioletRequiem();
    }

    @Test
    public void testGetValue() {
        assertEquals(910, equipment.getValue());
    }

    @Test
    public void testGetName() {
        assertEquals("Violet Requiem", equipment.getName());
    }

    @Test
    public void testGetDesc() {
        assertEquals("This is Violet Requiem", equipment.getDescription());
    }

}
