package id.ac.ui.cs.advprog.nightofknights.core.gamechar;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.nightofknights.core.stats.BossStats;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BossTest {

    private Class<?> bossClass;
    private Boss boss;

    @BeforeEach
    public void setUp() throws Exception {
        boss = Boss.getInstance();
        bossClass = Class.forName(Boss.class.getName());
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(bossClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstance() {
        assertEquals(boss, Boss.getInstance());
        assertEquals(boss.getClass(), bossClass);
    }

    @Test
    public void testGetCharStats() {
        assertNotNull(boss.getCharStats());
    }

    @Test
    public void testSetCharStats() {
        BossStats bossStats2 = new BossStats();
        boss.setCharStats(bossStats2);
        assertEquals(bossStats2, boss.getCharStats());
    }

    @Test
    public void testResetCharStats() {
        BossStats bossStats2 = (BossStats) boss.getCharStats();
        boss.resetCharStats();
        assertNotEquals(bossStats2, boss.getCharStats());
    }

    @Test
    public void testDeleteInstance() {
        Boss.deleteInstance();
        assertNotEquals(boss, Boss.getInstance());
    }
}
