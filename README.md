# Night Of Knights
Master : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/master/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/master)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/master/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/master)

Nadhif : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/Nadhif/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Nadhif)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/Nadhif/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Nadhif)

Jovi : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/Jovi/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Jovi)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/Jovi/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Jovi)

Timothy : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/timothy/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/timothy)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/timothy/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/timothy)

Roy : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/RoyGodsend/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/RoyGodsend)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/RoyGodsend/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/RoyGodsend)

Service-Registry : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/Service-Registry/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Service-Registry)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/Service-Registry/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Service-Registry)

Game-Service : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/Game-Service/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Game-Service)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/Game-Service/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/Game-Service)

History-Service : [![pipeline status](https://gitlab.com/EaglesCommander/night-of-knights/badges/History-Service/pipeline.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/History-Service)[![coverage report](https://gitlab.com/EaglesCommander/night-of-knights/badges/History-Service/coverage.svg)](https://gitlab.com/EaglesCommander/night-of-knights/-/commits/History-Service)

Final Project
CSCM602023 Advanced Programming @ Faculty of Computer Science Universitas
Indonesia, Term 2 2019/202

## Team

1. Nadhif Adyatma Prayoga - 1806205501  
2. Roy Godsend Salomo - 1806205230
3. Jovi Handono Hutama - 1806205161
4. Timothy Regana Tarigan - 1806205041

## Description

Untuk proyek akhir ini, kami akan membuat game text-based berbasis SpringBoot dengan platform website. 
Game ini bercerita tentang seorang knight yang harus mengalahkan sebuah boss yang mempunyai health yang sangat besar, maka knight itu harus jugalah bertambah kuat dengan mengurangi health boss sedikit demi sedikit.

Proyek ini akan memuat 4 fitur utama : buat karakter, upgrade equipment, change equipment, boss fight.

## Deployment

Main game
http://nightofknights.herokuapp.com

Server
http://nightofknightserver.herokuapp.com

History
http://nightofknighthistory.herokuapp.com

## Design Pattern

Kami akan menggunakan 4 design pattern :

1. Abstract Factory (Knight creation) (Roy)
2. Decorator (Upgrade Equipment) (Jovi)
3. Singleton (Boss dan Knight) (Nadhif)
4. Strategy (Change Equipment) (Timothy)

## Game Flow

Pertama - tama anda akan tiba di site (main screen) dan anda akan mempunyai 2 pilihan, yaitu new game dan continue.

Jika anda memilih new game maka anda akan menghapus apapun objek knight yang mungkin pernah ada di sistem dan mereplacenya dengan yang baru. Namun jika anda pilih continue maka anda akan melanjutkan dengan objek knight yang ada.

Setelah new game, anda akan tiba di screen pembuatan karakter, anda dapat menentukan nama dan equipment yang anda inginkan untuk karakter anda. Equipment yang anda dapat pakai bergantung dengan class yang anda pilih. Satu class hanya dapat memilih 2 jenis equipment.

Setelah itu anda akan tiba di home screen, di home screen anda dapat memilih untuk pergi ke shop screen, stats screen, battle screen atau kembali ke main screen.
Objektif game ini adalah untuk melawan dan membunuh boss.

Jika anda memilih battle screen untuk pertama kalinya, maka sebuah entity boss akan dibuat dengan konsep singleton, untuk subsequent access maka anda akan mendapat entity yang sama. Anda dapat melawan boss dan mencoba untuk mengurangi Health nya sampai 0. Sembari melawan anda akan mendapat Exp yang sesuai dengan total darah yang dikurangi. Anda dapat memilih untuk kembali ke home untuk meningkatkan kekuatan karakter anda. Jika darah anda mencapai 0 pada screen ini maka game over akan terjadi dan anda harus membuat karakter baru.
Jika anda memilih shop, anda dapat menukarkan Exp yang anda dapat untuk mengupgrade equipment atau karakter anda. Equipment dapat di upgrade sehingga bonus dari equipment tersebut bertambah. Anda juga dapat mengupgrade health dan mana dari karakter anda. Upgrade akan menumpuk satu sama lainnya.
Jika anda memilih stats, maka anda dapat melihat equipment, class, nama karakter, dan exp anda. Anda dapat melihat seberapa anda telah mengupgrade dan dan merubah equipment anda sesuai dengan kelas anda jika diperlukan.

Proses melawan - lari - upgrade akan diulang sampai salah satu dari health karakter menjadi 0, antara boss atau karakter anda. Setiap kali anda mengakses page health boss akan sama.

Setelah game berakhir, anda dapat membuat karakter baru lagi dari main screen.

## Main Screen

Assignee : Roy

Fitur :

1. New Game - Memulai sesi game baru, fitur ini akan mengoverride knight yang sebelumnya ada dan membuat entity baru
2. Continue - Fitur ini akan mengecek apakah ada knight yang sudah ada dan masih hidup sebelumnya, jika tidak maka tidak akan ada yang terjadi. Jika iya maka game dilanjutkan dengan entity tersebut

## Create Character Screen

Assignee : Roy

Anda akan dialihkan ke halaman pembuatan karakter jika anda menekan tombol “NEW GAME”.

Fitur yang ada dalam Create Character antara lain :

1. Mengisi nama karakter yang ingin dibuat.
2. Memilih class dari karakter yang ingin dibuat (Knight, Mage Knight, dan MarksMan Knight).
3. Memilih weapon,skill dan armor yang ingin digunakan (disesuaikan dengan class yang dipilih).
4. Menampilkan deskripsi weapon,skill dan armor yang tersedia untuk digunakan.

## Home Screen

Assignee : Timothy

Setelah anda Create Character atau setelah anda kembali dari screen yang lain. Anda akan tiba di screen ini. Fitur yang terdapat antara lain :

1. Shop : Berganti halaman ke Shop Screen
2. Stats : Berganti halaman ke Stats Screen
3. Boss : Berganti halaman ke Battle Screen
4. Exit : Berganti halaman ke Main Screen

## Shop Screen

Assignee : Jovi

Anda akan tiba di screen ini pada saat anda mengklik tombol “Shop” pada home

Fitur yang ada dalam Shop adalah :

1. Menambahkan bonus yang ada pada equipment (Upgrade)
2. Upgrade dapat dilakukan untuk weapon, armor, dan skill
3. Upgrade yang dilakukan dapat ditumpuk dengan upgrade sebelumnya
4. Upgrade hanya berlaku untuk suatu weapon/armor/skill tertentu
5. Upgrade dapat dilakukan menggunakan Exp

## Stats Screen

Assignee : Timothy

Anda akan tiba di screen ini pada saat anda mengklik tombol “Stats” pada home

Fitur yang ada dalam Stats adalah :

1. Melihat status dari karakter, antara lain berupa nama, class, exp, dan status equipment
2. Melihat upgrade yang dilakukan terhadap equipment
3. Anda dapat merubah equipment sesuai class yang dipilih

## Battle Screen

Assignee : Nadhif

Anda akan tiba di screen ini pada saat anda mengklik tombol “Boss” pada home

Fitur yang ada dalam Battle Screen adalah :

- Pembuatan boss (singleton) pada saat pengaksesan halaman pertama kalinya. Setelah itu boss yang keluar merupakan objek yang sama.
- Turn based combat yang dimulai dari karakter anda, lalu boss.
- Pada turn karakter anda, anda dapat melakukan : 
- Attack : Menyerang boss dengan mengambil attack dari weapon yang di equip dan modifiernya, serangan akan mengurangi HP boss
- Defend : Bertahan dengan mengambil defense dari armor yang di equip dan modifiernya, bertahan akan mengurangi damage yang diterima
- Skill : Menggunakan skill yang dipilih
- Run : Kembali ke home, mengakhiri encounter dengan boss

Penjelasan Skill :

- Skill yang dapat digunakan sesuai dengan skill yang dipilih

Penjelasan Stats Char :

- Health : Menunjukan tingkat kehidupan karakter, jika 0 maka game akan berakhir
- Mana : banyaknya jumlah skill yang dapat digunakan
- Exp : Banyaknya currency yang didapat dari boss yang dapat ditukar dengan upgrade di shop

Pada turn boss :

- Boss bisa attack karakter dimana boss akan mendapat value dari base value yang ada dalam objek boss dikali dengan level. Jenis attack dapat berbeda
- Boss dapat menggunakan move yang lebih kuat sewaktu - waktu tergantung random chance

Penjelasan stats boss :

- Health : Penanda kehidupan boss, jika 0 maka game akan berakhir dengan kemenangan karakter, health ini tidak akan reset kecuali game baru dimulai
- Level : Menunjukan tingkat kekuatan boss, akan naik setiap kali karakter mengakses battle screen